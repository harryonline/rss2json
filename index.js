var xml2js = require('xml2js-es6-promise');
var rp = require('request-promise-native');
var entities = require('html-entities');

exports.handler = (event, context, callback) => {
  var rss = event.queryStringParameters && event.queryStringParameters.rss_url;
  if (!rss) {
    callback(null, {
      statusCode: 400,
      headers: {"Content-Type": "application/json"},
      body: JSON.stringify({error: "No RSS url given"})
    });
    return;
  }
  convert(rss).then(result => callback(null, {
    statusCode: 200,
    headers: {"Content-Type": "application/json"},
    body: JSON.stringify(result, null, 2)
  })).catch(err => callback(err))
};

function convert(url) {
  return new Promise((resolve, reject) => {
    rp(url)
      .then(xml => xml2js(xml))
      .then(json => {
        var channel = json.rss.channel[0].title[0];
        var item = json.rss.channel[0].item[0];
        var quote = entities.decodeEntity(item.title[0].replace( /\s{2,}/, ' '), { level:'xml' });
        var description = item.description[0];
        var result = {
          url,
          quote,
          source: `${channel} - ${description}`
        };
        resolve(result);
      })
      .catch(err => reject(err))
  })
}

// var rss = 'http://wertarbyte.de/gigaset-rss/?long=1&limit=140&cookies=1&lang=en&format=rss&jar_id=95780971304118053647396689196894323976171195136475134';

// function json(err, data) {
//   console.log(data.body);
// }

// function test(rss) {
//   var event = {
//     queryStringParameters: {
//       rss_url: rss
//     }
//   }
//   exports.handler(event, undefined, json);
// }

// test(rss);
